vim.cmd("set nobackup nowritebackup")
vim.cmd("set clipboard=unnamedplus")
vim.cmd("set cmdheight=1")
vim.cmd("set completeopt=menuone,noselect")
vim.cmd("set conceallevel=0")
vim.cmd("set foldmethod=manual")
vim.cmd("set foldexpr=")
vim.cmd("set hidden")
vim.cmd("set hlsearch")
vim.cmd("set ignorecase")
vim.cmd("set mouse=a")
vim.cmd("set pumheight=10")
vim.cmd("set noshowmode")
vim.cmd("set cursorline")

-- Enable hybrid linenumbers & signcolumn
vim.cmd("set number")
vim.cmd("set relativenumber")
vim.cmd("set signcolumn=yes")
vim.cmd("set numberwidth=4")

vim.cmd("set nowrap")
vim.cmd("set scrolloff=8")
vim.cmd("set sidescrolloff=8")
vim.cmd("set noshowcmd")
vim.cmd("set noruler")
vim.cmd("set laststatus=3")

vim.cmd("set splitbelow")
vim.cmd("set splitright")
vim.cmd("set smartcase")
vim.cmd("set noswapfile")
vim.cmd("set termguicolors")
vim.cmd("set title")
vim.cmd("set undofile")
vim.cmd("set updatetime=100")

-- Tab is 2 spaces
vim.cmd("set nowritebackup")
vim.cmd("set expandtab")
vim.cmd("set shiftwidth=2")
vim.cmd("set tabstop=2")

-- Netrw
vim.g.netrw_browse_split = 0
vim.g.netrw_banner = 0
vim.g.netrw_winsize = 25
vim.g.netrw_altv = 1

